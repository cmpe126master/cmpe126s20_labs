#include <string>
#include "../inc/recursion.h"

namespace lab7 {
    recursion::recursion() {
    }

    recursion::~recursion() {
    }

    int recursion::fibonacci(int N) {
        return 0;
    }

    int recursion::climb_stairs(int n) {
        return 0;
    }

    int recursion::pow(int base, int n) {
        return 0;
    }

    // hint: first check the first and last characters, then recurse with what's left inside
    bool recursion::valid_parenthesis(std::string input) {
        return false;
    }

    // hint: this is very similar to what you've done with linked lists previously, just using recursion.
    bool recursion::are_nodes_ok(const node* head) {
        return false;
    }
}